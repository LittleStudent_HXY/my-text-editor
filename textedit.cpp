#include "textedit.h"
#include "ui_textedit.h"
#include <QPixmap>
#include <QFileInfo>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QFontDatabase>
#include <QActionGroup>
#include <QColorDialog>

TextEdit::TextEdit(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TextEdit)
{
    ui->setupUi(this);
    QPixmap pix(16, 16);
    pix.fill(Qt::red);
    QIcon  icon(pix);
    ui->actionColor->setIcon(icon);
    ComboboxStyle = new QComboBox();
    ui->toolBarCombination->addWidget(ComboboxStyle);
    ComboboxStyle->addItem("标准");
    ComboboxStyle->addItem("项目符号 (●)");
    ComboboxStyle->addItem("项目符号 (○)");
    ComboboxStyle->addItem("项目符号 (■)");
    ComboboxStyle->addItem("编号 (1.2.3.)");
    ComboboxStyle->addItem("编号 (a.b.c.)");
    ComboboxStyle->addItem("编号 (A.B.C.)");
    ComboboxStyle->addItem("编号 (Ⅰ.Ⅱ.Ⅲ.)");
    ComboboxStyle->addItem("编号 (ⅰ.ⅱ.ⅲ.)");
    ComboboxStyle->setStatusTip("段落加标号或编号");

    comboboxFont = new QFontComboBox();
    ui->toolBarCombination->addWidget(comboboxFont);
    comboboxFont->setStatusTip("更改字体");
    connect(comboboxFont, SIGNAL(currentTextChanged(QString)), this, SLOT(textFont(QString)));

    ComboboxSize = new QComboBox();
    ComboboxSize->setEditable(true);
    ComboboxSize->setStatusTip("更改字号");
    ui->toolBarCombination->addWidget(ComboboxSize);
    foreach(int size, QFontDatabase::standardSizes())
    {
        ComboboxSize->addItem(QString::number(size));
    }
    connect(ComboboxSize, SIGNAL(currentTextChanged(QString)), this, SLOT(textFontSize(QString)));

    QActionGroup *aglinGroup = new QActionGroup(this);
    aglinGroup->addAction(ui->actionLeftAlign);
    aglinGroup->addAction(ui->actionCenter);
    aglinGroup->addAction(ui->actionRightAlign);
    aglinGroup->addAction(ui->actionJustify);
    connect(aglinGroup, SIGNAL(triggered(QAction*)), this, SLOT(textAlign(QAction*)));

    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newFile()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveFile()));
    connect(ui->actionSaveTo, SIGNAL(triggered()), this, SLOT(saveFileAs()));
    connect(ui->actionUndo, SIGNAL(triggered()), this, SLOT(undo()));
    connect(ui->actionRedo, SIGNAL(triggered()), this, SLOT(redo()));
    connect(ui->actionCopy, SIGNAL(triggered()), this, SLOT(copy()));
    connect(ui->actionPaste, SIGNAL(triggered()), this, SLOT(past()));
    connect(ui->actionCut, SIGNAL(triggered()), this, SLOT(cut()));
    connect(ui->actionBold, SIGNAL(triggered()), this, SLOT(textBold()));
    connect(ui->actionItalic, SIGNAL(triggered()), this, SLOT(textItalic()));
    connect(ui->actionUnderLine, SIGNAL(triggered()), this, SLOT(textUnderLine()));
    connect(ui->actionColor, SIGNAL(triggered()), this, SLOT(textColor()));



    move(200, 150);
    resize(800, 500);
}

TextEdit::~TextEdit()
{
    delete ui;
}

void TextEdit::newFile()
{
    DocFile *docFile = createDocFile();

    docFile->newFile();
    docFile->show();
}

void TextEdit::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("打开"), QString(), tr("HTML 文档 (*.htm *html);;所有文件(*.*)"));
    if (fileName.isEmpty())
    {
        return;
    }
    QMdiSubWindow *window = findDocFile(fileName);
    if (window)
    {
        ui->mdiArea->setActiveSubWindow(window);
        return;
    }
    //不存在则需创建一个窗口
    DocFile *docFile = createDocFile();
    if (docFile->loadFile(fileName))
    {
        ui->statusbar->showMessage("文件已经载入", 2000);
        docFile->show();
    }
    else
    {
        docFile->close();
    }
}

void TextEdit::saveFile(void)
{
    if (activeDocFile() && activeDocFile()->save())
    {
        ui->statusbar->showMessage(tr("保存成功"), 2000);
    }
}

void TextEdit::saveFileAs()
{
    if (activeDocFile() && activeDocFile()->saveAs())
    {
        ui->statusbar->showMessage(tr("保存成功"), 2000);
    }
}

void TextEdit::undo(void)
{
    if (activeDocFile())
    {
        activeDocFile()->undo();
    }
}

void TextEdit::redo(void)
{
    if (activeDocFile())
    {
        activeDocFile()->redo();
    }
}

void TextEdit::copy(void)
{
    if (activeDocFile())
    {
        activeDocFile()->copy();
    }
}

void TextEdit::past(void)
{
    if (activeDocFile())
    {
        activeDocFile()->paste();
    }
}

void TextEdit::cut(void)
{
    if (activeDocFile())
    {
        activeDocFile()->cut();
    }
}

void TextEdit::textBold(void)
{
    QTextCharFormat format;
    if (activeDocFile())
    {
        format.setFontWeight(ui->actionBold->isChecked()?QFont::Bold:QFont::Normal);
        activeDocFile()->mergeFormat(format);
    }
}

void TextEdit::textItalic(void)
{
    QTextCharFormat format;
    if (activeDocFile())
    {
        format.setFontItalic(ui->actionItalic->isChecked());
        activeDocFile()->mergeFormat(format);
    }
}

void TextEdit::textUnderLine(void)
{
    QTextCharFormat format;
    if (activeDocFile())
    {
        format.setFontUnderline(ui->actionUnderLine->isChecked());
        activeDocFile()->mergeFormat(format);
    }
}

void TextEdit::textFont(const QString &font)
{
    QTextCharFormat format;
    if (activeDocFile())
    {
        format.setFont(font, QTextCharFormat::FontPropertiesSpecifiedOnly);
        activeDocFile()->mergeFormat(format);
    }
}

void TextEdit::textFontSize(const QString &size)
{
    qreal fSize = size.toFloat();

    if (fSize > 0)
    {
        QTextCharFormat format;
        if (activeDocFile())
        {
            format.setFontPointSize(fSize);
            activeDocFile()->mergeFormat(format);
        }
    }
}

void TextEdit::textAlign(QAction *action)
{
    if (action == ui->actionLeftAlign)
    {
        activeDocFile()->textAlign(1);
    }
    else if (action == ui->actionCenter)
    {
        activeDocFile()->textAlign(2);
    }
    else if (action == ui->actionRightAlign)
    {
        activeDocFile()->textAlign(3);
    }
    else if (action == ui->actionJustify)
    {
        activeDocFile()->textAlign(4);
    }
}

void TextEdit::textColor(void)
{
    DocFile *docFile = activeDocFile();
    if (docFile)
    {
        QColor color = QColorDialog::getColor(docFile->textColor(), this);
        if (!color.isValid())
        {
            return;
        }
        QTextCharFormat format;
        QBrush brush = QBrush(color);
        format.setForeground(brush);
        docFile->mergeFormat(format);
    }
}

DocFile *TextEdit::createDocFile(void)
{
    DocFile *docFile = new DocFile;

    ui->mdiArea->addSubWindow(docFile);
    connect(docFile, SIGNAL(copyAvailable(bool)), ui->actionCopy, SLOT(setEnabled(bool)));
    connect(docFile, SIGNAL(copyAvailable(bool)), ui->actionCut, SLOT(setEnabled(bool)));

    return docFile;
}

DocFile *TextEdit::activeDocFile()
{
    QMdiSubWindow *window = 0;

    window = ui->mdiArea->activeSubWindow();
    if (window)
    {
        return qobject_cast<DocFile *>(window->widget());
    }
    return 0;
}

QMdiSubWindow *TextEdit::findDocFile(const QString &fileName)
{
    QString canoniacalFilePath = QFileInfo(fileName).canonicalFilePath();
    foreach(QMdiSubWindow *window, ui->mdiArea->subWindowList())
    {
        DocFile *docFile = qobject_cast<DocFile *>(window->widget());
        if (docFile->currentFIle() == canoniacalFilePath)
        {
            return window;
        }
    }
    return 0;
}
































