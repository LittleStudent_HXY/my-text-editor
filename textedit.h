#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QMainWindow>
#include <QMdiArea>
#include <QComboBox>
#include <QFontComboBox>
#include "docfile.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TextEdit; }
QT_END_NAMESPACE

class TextEdit : public QMainWindow
{
    Q_OBJECT

public:
    TextEdit(QWidget *parent = nullptr);
    ~TextEdit();

private slots:
    void newFile(void);
    void openFile(void);
    void saveFile(void);
    void saveFileAs(void);
    void undo(void);
    void redo(void);
    void copy(void);
    void past(void);
    void cut(void);
    void textBold(void);
    void textItalic(void);
    void textUnderLine(void);
    void textFont(const QString &font);
    void textFontSize(const QString &size);
    void textAlign(QAction *action);
    void textColor(void);

private:
    QMdiSubWindow *findDocFile(const QString &fileName);
    DocFile *createDocFile(void);
    DocFile *activeDocFile(void);

private:
    Ui::TextEdit *ui;
    QComboBox *ComboboxStyle;
    QFontComboBox *comboboxFont;
    QComboBox *ComboboxSize;
};
#endif // TEXTEDIT_H
