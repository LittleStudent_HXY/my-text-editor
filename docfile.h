#ifndef DOCFILE_H
#define DOCFILE_H

#include <QTextEdit>

class DocFile : public QTextEdit
{
    Q_OBJECT
public:
    DocFile();
    void newFile();
    QString userFriendlyCurrentFIle(void);
    QString currentFIle(void) { return curFile; }
    bool    loadFile(const QString &fileName);
    void    setCurrentFileName(const QString &fileName);
    bool    save(void);
    bool    saveAs(void);
    bool    saveFile(QString fileName);
    void    mergeFormat(QTextCharFormat &charFormat);
    void    textAlign(int align);

protected:
    void closeEvent(QCloseEvent *event);
    bool toBeSaved(void);


private slots:
    void documentWasModified(void);

private:
    QString strippedName(QString &fullFileName);
    QString curFile;
    bool    isUntitled;
};

#endif // DOCFILE_H
